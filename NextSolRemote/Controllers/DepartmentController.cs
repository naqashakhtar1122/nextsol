﻿using NextSolRemote.Database;
using NextSolRemote.Repository.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NextSolRemote.Controllers
{
    public class DepartmentController : Controller
    {
        DepartmentRepo DeptRepo = new DepartmentRepo();
        // GET: Department
        public ActionResult Index()
        {
            var list = DeptRepo.GetAll();
            return View(list);
        }

        public bool AddUpdate(Department model)
        {
            if (model.id == 0)
            {
                DeptRepo.Add(model);
            }
            else
            {
                DeptRepo.Update(model);
            }
            return true;
        }

        public bool Delete(int Id)
        {
            DeptRepo.Delete(Id);
            return true;
        }
    }
}