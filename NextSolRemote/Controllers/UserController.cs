﻿using NextSolRemote.Database;
using NextSolRemote.Repository.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NextSolRemote.Controllers
{
    public class UserController : Controller
    {
        UserRepo UserRepo = new UserRepo();
        DepartmentRepo DeptRepo = new DepartmentRepo();

        public ActionResult Index()
        {
            var list = UserRepo.GetAll();
            ViewBag.Department = DeptRepo.GetAll().Where(f => f.Active == true).ToList();
            return View(list);
        }

        public bool AddUpdate(User model)
        {
            if (model.id == 0)
            {
                UserRepo.Add(model);
            }
            else
            {
                UserRepo.Update(model);
            }
            return true;
        }

        public bool Delete(int Id)
        {
            UserRepo.Delete(Id);
            return true;
        }
    }
}