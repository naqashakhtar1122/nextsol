﻿using NextSolRemote.Database;
using NextSolRemote.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NextSolRemote.Repository.Classes
{
    public class DepartmentRepo : IDepartment
    {
        public bool Add(Department department)
        {
            using (NextSolDBEntities db = new NextSolDBEntities())
            {
                try
                {
                    department.CreatedDate = department.UpdatedDate = DateTime.Now;
                    db.Departments.Add(department);
                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public bool Delete(int Id)
        {
            using (NextSolDBEntities db = new NextSolDBEntities())
            {
                try
                {
                    var existing = db.Departments.Find(Id);
                    if (existing != null)
                    {
                        db.Departments.Remove(existing);
                        db.SaveChanges();
                    }
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public List<Department> GetAll()
        {
            using (NextSolDBEntities db = new NextSolDBEntities())
            {
                return db.Departments.Include("Users").OrderBy(f => f.Name).ToList();
            }
        }

        public bool Update(Department department)
        {
            using (NextSolDBEntities db = new NextSolDBEntities())
            {
                try
                {
                    var existing = db.Departments.Find(department.id);
                    if (existing != null)
                    {
                        existing.Active = department.Active;
                        existing.Address = department.Address;
                        existing.CreatedDate = department.CreatedDate;
                        existing.UpdatedDate = existing.CreatedDate;
                        existing.Description = department.Description;
                        existing.Name = department.Name;
                        existing.UpdatedDate = DateTime.Now;
                        db.SaveChanges();
                    }
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
    }
}