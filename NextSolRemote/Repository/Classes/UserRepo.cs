﻿using NextSolRemote.Database;
using NextSolRemote.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace NextSolRemote.Repository.Classes
{
    public class UserRepo : IUser
    {
        public bool Add(User user)
        {
            using (NextSolDBEntities db = new NextSolDBEntities())
            {
                try
                {
                    user.CreatedDate = user.UpdatedDate = DateTime.Now;
                    db.Users.Add(user);
                    db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public bool Delete(int Id)
        {
            using (NextSolDBEntities db = new NextSolDBEntities())
            {
                try
                {
                    var existing = db.Users.Find(Id);
                    if (existing != null)
                    {
                        db.Users.Remove(existing);
                        db.SaveChanges();
                    }
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public List<User> GetAll()
        {
            using (NextSolDBEntities db = new NextSolDBEntities())
            {
               return db.Users.Include(x => x.Department).OrderBy(x => x.FirstName).ToList();
            }
        }

        public bool Update(User user)
        {
            using (NextSolDBEntities db = new NextSolDBEntities())
            {
                try
                {
                    var existing = db.Users.Find(user.id);
                    if (existing != null)
                    {
                        existing.Active = user.Active;
                        existing.Address = user.Address;
                        existing.Country = user.Country;
                        existing.CreatedDate = existing.CreatedDate;
                        existing.DepartmentID = user.DepartmentID;
                        existing.Description = user.Description;
                        existing.FirstName = user.FirstName;
                        existing.LastName = user.LastName;
                        existing.UpdatedDate = DateTime.Now;
                        db.SaveChanges();
                    }
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
    }
}