﻿using NextSolRemote.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NextSolRemote.Repository.Interface
{
    public interface IDepartment
    {
        List<Department> GetAll();
        bool Add(Department user);
        bool Update(Department department);
        bool Delete(int Id);
    }
}