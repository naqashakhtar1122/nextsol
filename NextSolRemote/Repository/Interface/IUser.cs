﻿using NextSolRemote.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NextSolRemote.Repository.Interface
{
    public interface IUser
    {
        List<User> GetAll();
        bool Add(User user);
        bool Update(User user);
        bool Delete(int Id);
    }
}